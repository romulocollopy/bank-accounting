defmodule BankAccounting.UseCasesTest do

  use BankAccounting.DataCase
  alias BankAccounting.Accounts.Account, as: Account
  alias BankAccounting.User, as: User
  alias BankAccounting.UseCases, as: UseCases
  alias BankAccounting.Repo, as: Repo
  alias Decimal, as: D

  test "transfer values from source_account to destination_account" do
    [source_account | destination_account] = account_fixture()
    [:ok, source_account, destination_account] = UseCases.transfer_funds source_account, destination_account, D.new(30)
    assert source_account.balance == D.new 12
    assert destination_account.balance == D.new 72
  end

  test "does not transfer values when source_account.balance is less then transfer value" do
    [source_account | destination_account] = account_fixture()
    [:error | message ] = UseCases.transfer_funds source_account, destination_account, D.new(50)
      assert message == "Insuficient funds"
  end

  test "does not transfer negative values and raises FunctionClauseError" do
    [source_account | destination_account] = account_fixture()
    [:error | message] = UseCases.transfer_funds source_account, destination_account, D.new(-12)
    assert message == "cannot transfer negative amounts"
  end

  def account_fixture do
    {:ok, user } = Repo.insert %User{
      name: "Paulo", document_type: "cpf", document_number: "123456789-25"
    }

    {:ok, source_account } = Repo.insert %Account{
      balance: D.new(42),
      identificator: "some identificator",
      user_id: user.id
    }

    {:ok, destination_account } = Repo.insert %Account{
      balance: D.new(42),
      identificator: "other identificator",
      user_id: user.id
    }

    [source_account | destination_account]
  end

end
