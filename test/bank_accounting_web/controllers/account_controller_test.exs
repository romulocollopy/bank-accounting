defmodule BankAccountingWeb.AccountControllerTest do
  use BankAccountingWeb.ConnCase

  alias BankAccounting.Accounts

  @create_attrs %{balance: "120.5", identificator: "some identificator"}

  def fixture(:account) do
    {:ok, account} = Accounts.create_account(@create_attrs)
    account
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all accounts", %{conn: conn} do
      conn = get conn, account_path(conn, :list_accounts)
      assert json_response(conn, 200)["data"] == []
    end
  end
end
