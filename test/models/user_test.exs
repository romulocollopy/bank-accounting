defmodule BankAccounting.UserTest do
  use BankAccounting.DataCase

  alias BankAccounting.User, as: User
  alias BankAccounting.Repo, as: Repo

  @valid_attrs %{document_number: "42912365409", document_type: "cpf", name: "some name"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "do not insert two users with same document" do
    changeset = User.changeset(%User{}, @valid_attrs)
    {:ok, user} = Repo.insert changeset
    assert user

    {:error, changeset} = Repo.insert changeset
    refute changeset.valid?
  end
end
