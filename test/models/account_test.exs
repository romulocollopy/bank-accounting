defmodule BankAccounting.AccountTest do
  use BankAccounting.DataCase

  alias BankAccounting.Accounts.Account, as: Account
  alias BankAccounting.Repo, as: Repo

  @valid_attrs %{balance: 42.0, identificator: "some identificator"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Account.changeset(%Account{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Account.changeset(%Account{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "chanset with duplicated identificator" do
    changeset = Account.changeset(%Account{}, @valid_attrs)
    {:ok, account} = Repo.insert changeset
    assert account

    {:error, changeset} = Repo.insert changeset
    refute changeset.valid?
  end
end
