# BankAccounting

This is my first application in Elixir / Phoenix.
It provides an api to transfer funds between accounts and check their balance, as descripted in https://gist.github.com/bezelga/f4f6c065a454665122b875b1566d5178.

For convenience it also provides an index capable of interacting with the api. It's a Proof of Concept running with VueJS framework.

The Phoenix app is very straigthforwards: it accepts api calls, validates data and delegates to an `Use Case` the execution of the business rules.

The avaliable api routes are:
- account_path  GET   /api/accounts                BankAccountingWeb.AccountController :list_accounts
- account_path  POST  /api/accounts/make-transfer  BankAccountingWeb.AccountController :make_transfer
- account_path  GET   /api/accounts/:id            BankAccountingWeb.AccountController :get_balance

The `:make_transfer` routes expects a JSON payload as follows, where the id is the actual database id, not the "identificator", shown as accout title in the frontend.

```json
{"amount": 20
"destination_account_id": 3
"source_account_id": 2}
```

The `:get_balance` route gets the balance of the account in the database, and is avaliable in the frontend in a redundant way to fullfill the given project description.

## #TODO:
- wrap the transfer in a `Repo.transaction`
- increase the test coverage
- record transactions events stream

## Install and run
To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
