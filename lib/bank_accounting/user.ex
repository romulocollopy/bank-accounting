defmodule BankAccounting.User do
  use Ecto.Schema
  import Ecto.Changeset


  schema "users" do
    field :document_number, :string
    field :document_type, :string
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:name, :document_number, :document_type])
    |> validate_required([:name, :document_number, :document_type])
    |> unique_constraint(:duplicated_document, name: :document_index)
  end
end
