defmodule BankAccounting.UseCases do
  alias BankAccounting.Accounts, as: AccountsRepo
  alias BankAccounting.Accounts.Account, as: Account
  alias Decimal, as: D

  def transfer_funds(source_account, destination_account, amount) do
    cond do
      D.negative? amount ->
        [:error | "cannot transfer negative amounts"]
      D.negative? D.compare(source_account.balance, amount) ->
        [:error | "Insuficient funds"]
      true ->
        # do_transfer should go inside a Repo.transaction
        [:ok, source_account, destination_account] = do_transfer source_account, destination_account, amount
    end
  end

  defp do_transfer(source_account, destination_account, amount) do
    source_change = Account.debit source_account, amount
    destination_change = Account.credit destination_account, amount

    {:ok, source_account} = AccountsRepo.update_account source_account, source_change
    {:ok, destination_account} = AccountsRepo.update_account destination_account, destination_change
    [:ok, source_account, destination_account]
  end
end
