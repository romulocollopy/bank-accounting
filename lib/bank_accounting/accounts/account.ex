defmodule BankAccounting.Accounts.Account do
  use Ecto.Schema
  import Ecto.Changeset
  alias Decimal, as: D


  schema "accounts" do
    field :balance, :decimal
    field :identificator, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def changeset(account, attrs) do
    account
    |> cast(attrs, [:balance, :identificator])
    |> validate_required([:balance, :identificator])
    |> unique_constraint(:duplicated_account, name: :identificator_unique)
  end

  def transfer_changeset(
    %{
      "source_account_id" => source_account_id,
      "destination_account_id" => destination_account_id,
      "amount" => amount
    }) do
    valid = true
    parsed_amount =  D.parse amount
    parsed_source_account_id = Integer.parse source_account_id
    parsed_destination_account_id = Integer.parse destination_account_id

    amount = case parsed_amount do
      {:error, _} ->
        D.new(-1)
      {:ok, value} ->
        value
    end

    unless !D.negative? amount do
      valid = false
    end

    source_account_id = case parsed_source_account_id do
      :error ->
        valid = false
      {id, _} ->
        id
    end

    destination_account_id = case parsed_destination_account_id do
      :error ->
        valid = false
      {id, _} ->
        id
    end

    case valid do
      true ->
        %{amount: amount, source_account_id: source_account_id, destination_account_id: destination_account_id}
      false ->
        :error
    end

  end

  def debit(account, amount) do
    case D.negative? amount do
      false -> %{balance: D.sub(account.balance, amount)}
      true -> :error
    end
  end

  def credit(account, amount) do
    case D.negative? amount do
      false -> %{balance: D.add(account.balance, amount)}
      true -> :error
    end
  end
end
