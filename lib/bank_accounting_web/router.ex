defmodule BankAccountingWeb.Router do
  use BankAccountingWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BankAccountingWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  scope "/api", BankAccountingWeb do
    pipe_through :api
    get "/accounts", AccountController, :list_accounts
    post "/accounts/make-transfer", AccountController, :make_transfer
    get "/accounts/:id", AccountController, :get_balance
  end
end
