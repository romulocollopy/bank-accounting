defmodule BankAccountingWeb.PageController do
  use BankAccountingWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
