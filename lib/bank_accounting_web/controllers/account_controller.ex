defmodule BankAccountingWeb.AccountController do
  use BankAccountingWeb, :controller

  alias BankAccounting.Accounts
  alias BankAccounting.Accounts.Account
  alias BankAccounting.UseCases, as: UseCases

  action_fallback BankAccountingWeb.FallbackController

  def list_accounts(conn, _params) do
    accounts = Accounts.list_accounts()
    render(conn, "index.json", accounts: accounts)
  end

  def show(conn, %{"id" => id}) do
    account = Accounts.get_account!(id)
    render(conn, "show.json", account: account)
  end

  def get_balance(conn, %{"id" => id}) do
    account = Accounts.get_account!(id)
    json conn, %{balance: "BRL " <> to_string(account.balance)}
  end

  def make_transfer(conn, params) do
    changeset = Account.transfer_changeset params
    case changeset do
      :error ->
        conn
        |> put_status(:bad_request)
        |> json(%{message: "bad arguments"})
      %{
        :source_account_id => source_account_id,
        :destination_account_id => destination_account_id,
        :amount => amount
      } ->
        source_account = Accounts.get_account!(source_account_id)
        destination_account = Accounts.get_account!(destination_account_id)
        return = UseCases.transfer_funds source_account, destination_account, amount
        case return do
          [:ok | _ ] -> json(conn, %{message: "transfer succesful"})
          [:error | message ] ->
            conn
            |> put_status(:bad_request)
            |> json(%{message: message, amount: amount, avaliable: source_account.balance})
        end
    end
  end
end
