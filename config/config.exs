# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :bank_accounting,
  ecto_repos: [BankAccounting.Repo]

# Configures the endpoint
config :bank_accounting, BankAccountingWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "VHre8XtX15RSIZdjqhyBbwrChl0w3c4sPpyPPVSm0nW2PmoNf8dfMozAvEJRmb5v",
  render_errors: [view: BankAccountingWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BankAccounting.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
