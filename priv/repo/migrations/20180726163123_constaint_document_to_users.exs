defmodule BankAccounting.Repo.Migrations.ConstaintDocumentToUsers do
  use Ecto.Migration

  def change do
    create unique_index(:users, [:document_type, :document_number], name: :document_index)
  end
end
