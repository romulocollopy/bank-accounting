defmodule BankAccounting.Repo.Migrations.ConstraintAccountIdentificator do
  use Ecto.Migration

  def change do
    create unique_index(:accounts, [:identificator], name: :identificator_unique)
  end
end
