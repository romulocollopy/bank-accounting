defmodule BankAccounting.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :name, :string
      add :document_number, :string
      add :document_type, :string

      timestamps()
    end

  end
end
