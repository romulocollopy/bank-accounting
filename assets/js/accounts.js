import Vue from './vue.min.js';
import Axios from 'axios';
import $ from 'jquery';
import 'bootstrap';

function getUrl(id){
    return document.getElementById(id).getAttribute("href");
}

var modal = $('#modal');

var sharedData = {
    urls: {
        list_accounts: getUrl('list-accounts'),
        make_transfer: getUrl('make-transfer'),
        get_balance: getUrl('get-balance')
    },
    modal: modal
};

var app = new Vue({
    el: "#accounts-app",
    data: function(){
        return {
            accounts: [],
            common: sharedData,
            source_account: null,
            destination_account: null,
            amount: null
        };
    },
    beforeMount: function(){
        var _this = this;
        Axios.get(this.common.urls.list_accounts)
            .then(function(resp){
                _this.accounts = resp.data.data;
            });
    },
    methods: {
        openModal: function(account){
            this.source_account = account;
            this.common.modal.modal('show');
        },
        closeModal: function(){
            this.source_account = null;
            this.common.modal.modal('hide');
        },
        getCurrentBalance: function(id){
            var url = this.common.urls.get_balance.replace('_id', id.toString());
            Axios.get(url)
                .then(function(resp){
                    alert(resp.data.balance);
                });
        },
        transfer: function(){
            var _this = this;
            Axios
                .post(
                    this.common.urls.make_transfer,
                    {
                        source_account_id: _this.source_account.id.toString(),
                        destination_account_id: _this.destination_account.id.toString(),
                        amount: _this.amount
                    }
                )
                .then(function(resp){
                    var source_account, destination_account, account;
                    for (let i = _this.accounts.length - 1 ; i >= 0; i--){
                        account = _this.accounts[i];
                        if(account.id == _this.source_account.id){
                            source_account = account;
                        };
                    }
                    for (let i = _this.accounts.length - 1; i >= 0; i--){
                        account = _this.accounts[i];
                        if(account.id == _this.destination_account.id){
                            destination_account = account;
                        };
                    }
                    source_account.balance = parseFloat(source_account.balance) - parseFloat(_this.amount);
                    destination_account.balance = parseFloat(destination_account.balance) + parseFloat(_this.amount);
                    _this.closeModal();
                })
                .catch(function(req, err){
                    alert(req.response.data.message);
                });
        }
    }
});
